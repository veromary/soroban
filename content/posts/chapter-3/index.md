---
title: "Conventions in Illustrations"
date: 2023-06-14T12:15:42+10:00
---

From The Japanese abacus : its use and theory
by Kojima, Takashi, available to borrow [from the Internet Archive](https://archive.org/details/japaneseabacusit00koji)

![Key to Illustrations](key.jpg)

This seems like a good system, though [pgf-soroban](https://ctan.org/pkg/pgf-soroban) doesn't seem to have any way to add arrows.



