---
title: "Read It"
date: 2023-02-08T10:33:54+11:00
---

So, here we look at reading numbers from a soroban.

![Let's review how to read the abacus](screen.jpg)

[read-it.pdf](read-it.pdf)

Also had a start at a Drill Sheet.

[drill.pdf](drill.pdf)

