---
date: 2023-02-05T10:58:08-04:00
description: "Getting Started"
tags: ["scene"]
title: "How to use pgf-soroban"
---

Well, setting up a simple Hugo blog took a little longer than expected. I also moved the /home directory to the removable SD card, which frees up more of my tiny laptop's hard drive, but also introduces lags as the writing to an SD card is slower than the built in flash drive. Maybe I should add a laptop to my [Buy Me A Coffee Wishlist](https://www.buymeacoffee.com/veronicab)...

Anyway, now we can get back to actually making some worksheets!

[chapter-1.pdf](chapter-1.pdf)

And as a side project, I've figured out how to make a 8 page folding mini-book!

[minibook.pdf](minibook.pdf)

I used two iterations of pdfpages: an [intermediate.tex](intermediate.tex) and then the final [minibook.tex](minibook.tex).

I'm very chuffed.



